Title: URL lookup service (urlinfo)
Author: Nathan Blok

## Description
Service that maintains several databases of malware URL's. If the resource being requested from the service 
is known to contain malware, then the service will pass back this information.

## Use
A GET requests where the caller passes in a URL and the service responds with some information about that URL. 
The GET request should  look like this:

`GET /urlinfo/1/{hostname_and_port}/{original_path_and_query_string}`

## Install and Run Directions
1. Clone repository
   Run: git clone https://nblok@bitbucket.org/nblok/urlinfo.git urlinfo
2. Switch to repository directory
   Run: cd urlinfo
3. Run: vagrant up --provision
4. Run: vagrant ssh
5. Run: cd /vagrant
6. Run: ./setup.sh
7. Run Tests: ./run_tests.sh
8. Hit service from browser:
    `http://10.100.199.228/urlinfo/1/000freexxx.com:80`
    `http://10.100.199.228/urlinfo/1/000freexxx.com:80/some/path`
   
9. Hit service from curl(run from withing vm created by vagrant):
   `curl -i -H "Accept: application/json" "http://10.100.199.228/urlinfo/1/nblok.ca:80"`
   `curl -i -H "Accept: application/json" "http://10.100.199.228/urlinfo/1/nblok.ca:80/somepath"`
   `curl -i -H "Accept: application/json" "http://10.100.199.228/urlinfo/1/000freexxx.com:80"`
   `curl -i -H "Accept: application/json" "http://10.100.199.228/urlinfo/1/000freexxx.com:80/some/path"`
   `curl -i -H "Accept: application/json" "http://10.100.199.228/urlinfo/1/someunknown.com:80"`
   
## Notes
* Query parameters are currently not taken into consideration
* If path matches then action for that bath is used, otherwise will
 see if there is a match for the domain and port
* If nothing is found then action will be `unknown` instead of one of
 `allow`, `block`
10. Load more urls (from with vagrant vm)
`./load_urls.sh blacklist/blacklist_<number>.csv`

see csv files in urlinfoapi/blacklist or urlinfoapi/whitelist for those 
that can be loaded. Most of the blacklist files should be about 10000
entries (on my machine they took about 3 secs. to load)
 

## Design Considerations
* The size of the URL list could grow infinitely, how might you scale this beyond the memory capacity of the system
* The number of requests may exceed the capacity of this system. How could the system be scaled to handle more requests.
* What strategies could be used to update the service with new URLs. Updates could happen multiple times a day and could 
have thousands of new urls at a time.