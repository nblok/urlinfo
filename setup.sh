#!/bin/bash

docker-compose build && docker-compose up -d

# a little time to breath
sleep 3

# create cassandra schema
docker-compose exec urlinfo python3 manage.py create_schema

# load some data
docker-compose exec urlinfo python3 manage.py urlload -f blacklist/blacklist_000.csv
docker-compose exec urlinfo python3 manage.py urlload -f whitelist/whitelist.csv
