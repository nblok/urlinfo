#!/usr/bin/env python
import os
import shutil
import csv
import re
from subprocess import call

# extract blacklist
# first need to download this file from
# http://www.shallalist.de/Downloads/shallalist.tar.gz
BLACKLIST_FILE = 'shallalist.tar.gz'
BLACKLIST_DIR = 'BL'
BLACKLIST_FILE_SIZE = 10000
call(['tar', '-xzvf', BLACKLIST_FILE])

def read_url_data(abs_file_path):
    with open(abs_file_path, 'r', encoding="ISO-8859-1") as fh:
        for line in fh.readlines():
            line = line.strip()
            # skipping ip addresses
            if ip_pattern.match(line):
                continue

            yield [line.strip(), category.strip('/ '), 'block']

def write_url_data(outfile_num, outfile_lines):
    with open('./blacklist_{0:0>3}.csv'.format(outfile_num), 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
        writer.writerows(outfile_lines)

blacklist_dir_path = os.path.join(os.getcwd(), BLACKLIST_DIR)
ip_pattern = re.compile("^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}")
count = 0
outfile_lines = []
outfile_num = 0
if os.path.exists(blacklist_dir_path):
    for root, directories, filenames in os.walk(blacklist_dir_path):
        if len(filenames) > 0 and 'COPYRIGHT' not in filenames:
            category = root[len(blacklist_dir_path):]
            for file in filenames:
                abs_file_path = os.path.join(root, file)
                for url_data in read_url_data(abs_file_path):
                    outfile_lines.append(url_data)
                    if (count + 1) % BLACKLIST_FILE_SIZE == 0:
                        write_url_data(outfile_num, outfile_lines)
                        outfile_lines = []
                        outfile_num += 1

                    count += 1

    if len(outfile_lines) > 0:
        write_url_data(outfile_num, outfile_lines)


    shutil.rmtree(blacklist_dir_path)
else:
    print("Failed to find blacklist directory:\n{}".format(blacklist_dir_path))




