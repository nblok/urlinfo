import os
from flask_script import Manager, Server
from urlinfo import create_app
from urlinfo.commands import UrlLoaderCommand, CreateSchemaCommand

# default to dev config
env = os.environ.get('URLINFO_APP_ENV', 'dev')
app = create_app('urlinfo.config.%sConfig' % env.capitalize())

manager = Manager(app)
manager.add_command("server", Server(host='0.0.0.0', port=8080))
manager.add_command("urlload", UrlLoaderCommand())
# create the schema for the app
manager.add_command("create_schema", CreateSchemaCommand())

@manager.shell
def make_shell_context():
    return dict(app=app)

if __name__ == "__main__":
    manager.run()