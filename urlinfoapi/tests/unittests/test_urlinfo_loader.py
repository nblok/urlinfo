import os
import pytest

from urlinfo.lib.loader import UrlCsvLoader, LoaderException
from urlinfo import db
from urlinfo.commands import UrlLoaderCommand

URL_WITH_PORT_INDEX = 4
URL_WITH_PORT_EXPECTED_PORT = 8080
URL_WITH_PORT_EXPECTED_HOST = '1000femmes.com'
URL_EXPECTED_COUNT = 15

BLACKLIST_FILE = 'blacklist.csv'
BLACKLIST_INVALID_COLS_FILE = 'invalid_columns_blacklist.csv'


def test_loader_can_load_from_csv(test_data_dir):
    blacklist_csv_file = os.path.join(test_data_dir, BLACKLIST_FILE)
    loader = UrlCsvLoader(blacklist_csv_file)
    url_info_list = []
    for url_info in loader.read():
        url_info_list.append(url_info)

    assert len(url_info_list) == 15
    url_with_port = url_info_list[URL_WITH_PORT_INDEX]
    assert url_with_port.hostname_and_port == "{}:{}".format(URL_WITH_PORT_EXPECTED_HOST, URL_WITH_PORT_EXPECTED_PORT)

def test_loader_can_deal_with_invalid_number_of_columns(test_data_dir):
    blacklist_csv_invalid_columns = os.path.join(test_data_dir, BLACKLIST_INVALID_COLS_FILE)
    loader = UrlCsvLoader(blacklist_csv_invalid_columns)
    url_info_list = []

    with pytest.raises(LoaderException):
        for url_info in loader.read():
            url_info_list.append(url_info)

