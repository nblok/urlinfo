import pytest

from urlinfo.models import UrlInfo, UrlInfoException, ACTION_ALLOW

TEST_DOMAIN_NAME = 'nblok.ca'
TEST_DOMAIN_NAME_WITH_PORT = 'nblok.ca:80'
TEST_PATH_WITH_QUERY = '/about/me?page_id=10'
TEST_PATH_NO_QUERY = '/about/me'


def test_urlinfo_add_port_when_only_hostname_provided():
    # when port provided should use that port
    expected_with_port = '{}:8080'.format(TEST_DOMAIN_NAME)
    urlinfo = UrlInfo(expected_with_port, None)
    assert urlinfo.hostname_and_port == expected_with_port
    assert not urlinfo.has_path()
    assert urlinfo.hostname_port_and_path == expected_with_port

    # when port is not provided use default
    hostname_only = TEST_DOMAIN_NAME
    expected_name_only = TEST_DOMAIN_NAME_WITH_PORT
    urlinfo = UrlInfo(hostname_only, None)
    assert urlinfo.hostname_and_port == expected_name_only
    assert not urlinfo.has_path()

def test_urlinfo_remove_query_from_path():
    '''
        Testing that this removes the query part from path as well as leading slash.
        currently doing this just to make matching simpler.
    '''
    expected_path_and_query = 'about/me'
    urlinfo = UrlInfo(TEST_DOMAIN_NAME_WITH_PORT, TEST_PATH_WITH_QUERY)
    assert urlinfo.path_and_query == expected_path_and_query
    assert urlinfo.has_path()

def test_urlinfo_raise_exception_on_invalid_action():
    with pytest.raises(UrlInfoException):
        urlinfo = UrlInfo(TEST_DOMAIN_NAME_WITH_PORT, TEST_PATH_WITH_QUERY, action='invalid')

def test_urlinfo_action_and_category_can_be_populated():
    expected_category = 'finance'
    urlinfo = UrlInfo(TEST_DOMAIN_NAME_WITH_PORT, TEST_PATH_WITH_QUERY, action=ACTION_ALLOW, category=expected_category)
    assert urlinfo.category == expected_category
    assert urlinfo.action == ACTION_ALLOW
    assert urlinfo.has_path()
    assert urlinfo.hostname_port_and_path == "{}{}".format(TEST_DOMAIN_NAME_WITH_PORT, TEST_PATH_NO_QUERY)

def test_urlinfo_build_from_url():
    expected_category = 'finance'
    expected_path_and_query = 'about/me'
    urlinfo = UrlInfo.build_from_url("{}{}".format(TEST_DOMAIN_NAME_WITH_PORT, TEST_PATH_WITH_QUERY), expected_category,
                                     ACTION_ALLOW)
    assert urlinfo.hostname_and_port == TEST_DOMAIN_NAME_WITH_PORT
    assert urlinfo.path_and_query == expected_path_and_query
    assert urlinfo.category == expected_category
    assert urlinfo.action == ACTION_ALLOW
    assert urlinfo.has_path()

    urlinfo = UrlInfo.build_from_url("{}{}".format(TEST_DOMAIN_NAME, TEST_PATH_NO_QUERY), expected_category,
                                     ACTION_ALLOW)
    assert urlinfo.hostname_and_port == TEST_DOMAIN_NAME_WITH_PORT
    assert urlinfo.path_and_query == expected_path_and_query
    assert urlinfo.category == expected_category
    assert urlinfo.action == ACTION_ALLOW
    assert urlinfo.has_path()


