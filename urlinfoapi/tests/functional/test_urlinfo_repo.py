from urlinfo.models import UrlInfo, ACTION_BLOCK, ACTION_ALLOW


def test_urlinfo_repo_batch_save(repo):
    batch_future = repo.batch_save([
        UrlInfo('www.nblok.ca', category='finance', action=ACTION_BLOCK),
        UrlInfo('www.nblok.ca', path_and_query='/some/safe/path', category='sports/cycling', action=ACTION_ALLOW)
    ])

    result = batch_future.result()

    assert result is not None


def test_urlinfo_repo_find(repo):
    urlino_one = UrlInfo('www.nblok.ca', category='finance', action=ACTION_BLOCK)
    urlino_two = UrlInfo('www.nblok.ca', path_and_query='/some/safe/path', category='sports/cycling', action=ACTION_ALLOW)
    urlino_three = UrlInfo('www.nblok.ca', path_and_query='/some/unknown/path')
    urlino_four = UrlInfo('www.nblok.ca')
    repo.batch_save([
        urlino_one,
        urlino_two,
    ])

    urlino_one = repo.find(urlino_one)
    assert urlino_one.action == ACTION_BLOCK

    urlino_two = repo.find(urlino_two)
    assert urlino_two.action == ACTION_ALLOW

    urlino_three = repo.find(urlino_three)
    assert urlino_three.action == ACTION_BLOCK

    urlino_four = repo.find(urlino_four)
    assert urlino_four.action == ACTION_BLOCK


# def test_urlinfo_repo_truncate():
#     pass


