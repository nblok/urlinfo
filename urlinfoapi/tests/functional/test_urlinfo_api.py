import pytest
import os

from flask import url_for
from urlinfo.commands import UrlLoaderCommand
from urlinfo.models import ACTION_BLOCK, ACTION_ALLOW

@pytest.fixture(scope='module')
def urlinfo_module(test_data_dir):
    loader = UrlLoaderCommand()
    loader.run(os.path.join(test_data_dir, 'blacklist.csv'))

@pytest.mark.usefixtures('client_class')
class TestUrlInfoApi:

    def test_get_urlinfo_successfully_responds(self, repo, urlinfo_module):
        hostname_and_port = '1000femmes.com:8080'
        url = url_for('urlinfo.urlinforesource', hostname_and_port=hostname_and_port)

        resp = self.client.get(url)
        assert 'data' in resp.json

        data = resp.json['data'][0]
        assert data['hostname_and_port'] == hostname_and_port
        assert data['path_and_query'] is None
        assert data['action'] == ACTION_BLOCK

        hostname_and_port = 'nblok.ca:80'
        url = url_for('urlinfo.urlinforesource', hostname_and_port=hostname_and_port)
        resp = self.client.get(url)
        assert 'data' in resp.json

        data = resp.json['data'][0]
        assert data['hostname_and_port'] == hostname_and_port
        assert data['path_and_query'] is None
        assert data['action'] == ACTION_ALLOW




