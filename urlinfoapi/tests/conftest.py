import pytest
import sys
import os

basedir = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, basedir + '/../')

from urlinfo import create_app, db
from urlinfo.commands import CreateSchemaCommand
from urlinfo.dataaccess import UrlInfoRepo

@pytest.fixture(scope='session')
def app():
    test_app = create_app('urlinfo.config.TestConfig')

    return test_app

@pytest.fixture(scope='session')
def test_data_dir():
    return os.path.join(basedir, 'data')

@pytest.fixture(scope='session')
def repo(request, app):

    command = CreateSchemaCommand()
    command.run()

    _repo = UrlInfoRepo()

    def fin():
        _repo.clean()

    request.addfinalizer(fin)

    return _repo

