import logging

from cassandra.cluster import Cluster
from cassandra.io.libevreactor import LibevConnection
from cassandra.query import ConsistencyLevel

from flask import current_app, g
from flask_restful import Api
from flask_celery import Celery
from flask_redis import FlaskRedis
from flask_cassandra import CassandraCluster

try:
    unicode
except NameError:  # Python3
    unicode = str

rest_api = Api()
celery = Celery()
redis = FlaskRedis()

# monkey catch cassandra connect so can specify connection class
def connect(self, **kwargs):
    log = logging.getLogger('flask_cassandra')
    log.debug("Connecting to CASSANDRA NODES {}".format(current_app.config['CASSANDRA_NODES']))
    if self.cluster is None:
        if isinstance(current_app.config['CASSANDRA_NODES'], (list, tuple)):
            self.cluster = Cluster(current_app.config['CASSANDRA_NODES'], **kwargs)
        elif isinstance(current_app.config['CASSANDRA_NODES'], (str, unicode)):
            self.cluster = Cluster([current_app.config['CASSANDRA_NODES']], **kwargs)
        else:
            raise TypeError("CASSANDRA_NODES must be defined as a list, tuple, string, or unicode object.")

    online_cluster = self.cluster.connect()

    return online_cluster

CassandraCluster.connect = connect

cassandra = CassandraCluster()


class CassandraDatabase():
    def __init__(self, app=None):
        self._session = None
        self._app = app
        self.consistency_level = ConsistencyLevel.ONE
        self._session = None

    def init_app(self, app):
        self._app = app

    @property
    def session(self):
        with self._app.app_context():
            keyspace = self._app.config.get('URLINFO_KEYSPACE', 'urlinfo_keyspace')
            if self._session is None:
                self._session = g._cassandra_session =  cassandra.connect(
                    connection_class=LibevConnection, protocol_version=2)
                with open('./schema/keyspace.cql', 'r') as fh:
                    content = fh.read().format(keyspace=keyspace)
                    r = self._session.execute(content)

            self._session.set_keyspace(keyspace)
        return self._session


db = CassandraDatabase()

