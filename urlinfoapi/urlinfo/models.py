from urllib.parse import urlparse, unquote

ACTION_BLOCK = 'block'
ACTION_ALLOW = 'allow'
ACTION_UNKNOWN = 'unknown'

ALL_ACTIONS = [ACTION_BLOCK, ACTION_ALLOW, ACTION_UNKNOWN]
DEFAULT_PORT = 80


class UrlInfoException(Exception):
    pass


class UrlInfo:

    def __init__(self, hostname_and_port, path_and_query=None, category=None, action=None):
        hostname_and_port = hostname_and_port if hostname_and_port.find(':') != -1 else \
            "{}:{}".format(hostname_and_port, DEFAULT_PORT)

        self.hostname_and_port = hostname_and_port.lower()

        self.path_and_query = self._sanitize_path_and_query(path_and_query) if path_and_query is not None else \
                              path_and_query
        self.category = category
        self.action = action.strip() if action else None
        if action and action not in ALL_ACTIONS:
            raise UrlInfoException('Action must be one of "{}", got "{}"'.format(ALL_ACTIONS,action))

    def has_path(self):
        if self.path_and_query is None or not self.path_and_query.strip():
            return False
        return True

    @property
    def hostname_port_and_path(self):
        if self.has_path():
            return "{}/{}".format(self.hostname_and_port, self.path_and_query)
        else:
            return self.hostname_and_port

    @staticmethod
    def _sanitize_path_and_query(path_and_query):
        query_marker_index = path_and_query.find('?')
        path_and_query = path_and_query[:query_marker_index] if query_marker_index != -1 else path_and_query
        path_and_query = path_and_query.strip('/')
        return path_and_query

    @classmethod
    def build_from_url(cls, url, category, action):
        url = url if "://" in url else "http://" + url
        result = urlparse(url)
        path = result.path
        query = result.query
        hostname = result.netloc[4:] if result.netloc.lower().startswith('www.') else result.netloc
        path_and_query = path if query is None else "{}?{}".format(path, query)
        return cls(hostname, path_and_query, category=category, action=action)

    def __str__(self):
        return "UrlInfo: hostname/port: {}, path/query: {}, action: {}".format(
            self.hostname_and_port, self.path_and_query, self.action)

