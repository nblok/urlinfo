import collections

from cassandra import ReadTimeout
from cassandra.query import BatchStatement, ConsistencyLevel

from urlinfo import db
from urlinfo.models import ACTION_UNKNOWN

URLINFO_TABLE = 'urlinfo'
EMPTY_PATH_AND_QUERY = '##__NONE__##'


class UrlInfoRepoException(Exception):
    pass


class UrlInfoRepo():

    def batch_save(self, url_info_list):
        insert_stmt = db.session.prepare(
            'INSERT INTO {table} (hostname_port, path, action) VALUES (?, ?, ?)'.format(table=URLINFO_TABLE)
        )

        url_info_count = 0

        try:
            batch_insert = BatchStatement(consistency_level=db.consistency_level)
            for url_info in url_info_list:
                batch_insert.add(insert_stmt, (url_info.hostname_and_port,
                                               url_info.path_and_query if url_info.has_path() else EMPTY_PATH_AND_QUERY,
                                               url_info.action))
                url_info_count += 1
            future = db.session.execute_async(batch_insert)

            return future
        except Exception as e:
            raise UrlInfoRepoException('Batch save failed: {}'.format(e))

    def find(self, url_info):
        select_stmt = "SELECT hostname_port, path, action FROM {table} WHERE hostname_port=%s AND path IN (%s, %s)"

        future = db.session.execute_async(select_stmt.format(table=URLINFO_TABLE), (
            url_info.hostname_and_port, EMPTY_PATH_AND_QUERY,
            url_info.path_and_query if url_info.has_path() else EMPTY_PATH_AND_QUERY))

        try:
            hostname_to_path_action = collections.defaultdict(dict)
            rows = future.result()
            for row in rows:
                hostname_to_path_action[row.hostname_port][row.path] = row.action

            url_info.action = ACTION_UNKNOWN
            if url_info.hostname_and_port in hostname_to_path_action:
               if url_info.path_and_query in hostname_to_path_action.get(url_info.hostname_and_port, {}):
                   url_info.action = hostname_to_path_action[url_info.hostname_and_port][url_info.path_and_query]
               elif EMPTY_PATH_AND_QUERY in hostname_to_path_action.get(url_info.hostname_and_port, {}):
                   url_info.action = hostname_to_path_action[url_info.hostname_and_port][EMPTY_PATH_AND_QUERY]
        except ReadTimeout:
            raise UrlInfoRepoException('failed in find, read timeout')

        return url_info

    def clean(self):
        truncate_stmt = "TRUNCATE {table}"
        try:
            db.session.execute(truncate_stmt.format(table=URLINFO_TABLE))
        except Exception as e:
            raise UrlInfoRepoException('Clean failed: {}'.format(e))


