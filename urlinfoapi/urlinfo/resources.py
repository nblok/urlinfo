from flask import Blueprint
from flask_restful import Resource, marshal_with

from urlinfo.extensions import rest_api
from urlinfo.models import UrlInfo, ACTION_UNKNOWN
from urlinfo.dataaccess import UrlInfoRepo

url_info_bp = Blueprint('urlinfo', __name__, url_prefix='/urlinfo/1')


class UrlInfoResource(Resource):

    def get(self, hostname_and_port, path_and_query=None):

        url_info_repo = UrlInfoRepo()
        url_info = url_info_repo.find(UrlInfo(hostname_and_port, path_and_query))

        return {'data': [
            {
                "hostname_and_port": hostname_and_port,
                "path_and_query": path_and_query,
                "action": url_info.action,
            }
        ]}


rest_api.add_resource(
    UrlInfoResource,
    '/<string:hostname_and_port>',
    '/<string:hostname_and_port>/<path:path_and_query>')
rest_api.init_app(url_info_bp)

