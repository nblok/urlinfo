import time
import logging

from flask_script import Command, Option, Manager
from urlinfo.lib.loader import UrlCsvLoader
from urlinfo.dataaccess import UrlInfoRepo
from urlinfo import db

BATCH_SIZE = 5000

class UrlLoaderCommand(Command):
    """Command to load url list"""
    option_list = (
        Option('--file', '-f', dest='file_path', help='Path to csv file containing url list'),
    )

    def run(self, file_path):
        urlinfo_repo = UrlInfoRepo()
        batch_futures = []
        try:
            loader = UrlCsvLoader(file_path)

            urlinfo_list = []
            count = 1
            for urlinfo in loader.read():
                urlinfo_list.append(urlinfo)
                if count % BATCH_SIZE == 0:
                    logging.info('Modding and performing save')
                    # time.sleep(1)
                    future = urlinfo_repo.batch_save(urlinfo_list)
                    batch_futures.append(future)
                    urlinfo_list = []

                count += 1

            if len(urlinfo_list) > 0:
                logging.info('Performing last save')
                # time.sleep(1)
                future = urlinfo_repo.batch_save(urlinfo_list)
                batch_futures.append(future)

            # wait for result
            for future in batch_futures:
                res = future.result()

            print('URL\'s Loaded')

        except Exception as e:
            logging.error('Error occurred {}'.format(e))

class CreateSchemaCommand(Command):
    def run(self):
        # for file_suffix in ['hostname', 'hostname_port', 'hostname_port_path']:
        # with open('./schema/urlinfo_{}.cql'.format(file_suffix), 'r') as fh:
        with open('./schema/urlinfo.cql', 'r') as fh:
            content = fh.read()
            r = db.session.execute(content)


