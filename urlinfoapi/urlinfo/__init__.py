# url info api

from flask import Flask
from .config import DevConfig
from .extensions import rest_api, redis, celery, cassandra, db
from .resources import url_info_bp


def create_app(config_object_name):

    app = Flask(__name__)
    app.config.from_object(config_object_name)
    redis.init_app(app)
    celery.init_app(app)
    cassandra.init_app(app)
    db.init_app(app)

    @app.errorhandler(404)
    def page_not_found(e):
        # page not found response
        resp = """
        <html>
            <head>
                <title>urlinfo api</title>
            </head>
            <body>
                <h3>page not found</h3>
                <p>
                    resource available: /urlinfo/1/hostname_and_port/path_and_query
                </p>
            </body>
        </html>
        """
        return resp

    # adding resource api
    app.register_blueprint(url_info_bp)

    return app

