import pandas
import numpy
import logging
import csv

import urlinfo.models

from urlinfo.dataaccess import UrlInfoRepo

class LoaderException(Exception):
    pass

URLINFO_NUMBER_OF_COLUMNS = 3

class UrlCsvLoader():

    def __init__(self, data_file):
        self._data_file = data_file

    def read(self):
        data_frame = pandas.read_csv(self._data_file,
                                     names=['url', 'category', 'action'],
                                     dtype={'url': str, 'category': str, 'action': str},
                                     quoting=csv.QUOTE_ALL, quotechar='"')
        for row in data_frame.iterrows():
            url_data = row[1]
            url_data.action = str(url_data.action).strip() if url_data.action else None

            # TODO: add checks for url, and category columns
            if url_data.action is None or url_data.action not in urlinfo.models.ALL_ACTIONS:
                raise LoaderException("invalid actions column, got: {} expected one of: {}".format(
                                      url_data.action, urlinfo.models.ALL_ACTIONS))

            yield urlinfo.models.UrlInfo.build_from_url(url_data.url, url_data.category, url_data.action)






