
class Config:
    '''
        Base configuration class
    '''
    DEBUG = False
    TESTING = False
    SECRET_KEY = 'not_so_secret_key'
    REDIS_URL = 'redis://redis/0'
    CELERY_BROKER_URL = 'redis://redis/0'
    CELERY_RESULT_BACKEND = 'redis://redis/0'
    CELERY_IMPORTS = ('urlinfo.tasks')
    URLINFO_KEYSPACE = 'urlinfo_keyspace'
    CASSANDRA_NODES = ['cassandra-c1']


class ProdConfig(Config):
    pass


class DevConfig(Config):
    DEBUG = True
    SECRET_KEY = 'dev_not_so_secret_key'
    REDIS_URL = 'redis://redis/1'
    CELERY_BROKER_URL = 'redis://redis/1'
    CELERY_RESULT_BACKEND = 'redis://redis/1'


class TestConfig(DevConfig):
    DEBUG = True
    TESTING = True
    SECRET_KEY = 'test_not_so_secret_key'
    URLINFO_KEYSPACE = 'test_urlinfo_keyspace'

